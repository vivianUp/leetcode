import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ComposeInteger {
    //给一个整数数组（元素0-9），输出一个使用这些元素组成的List<Integer>,按照increase order排序

    public static List<Integer> getIntegers(int[] arr) {
        List<Integer> ret = new ArrayList<>();
        if (arr.length == 0) return ret;
        Arrays.sort(arr);
        boolean[] visited = new boolean[arr.length];
        helper(ret, arr, visited, new StringBuilder());
        return ret;
    }

    private static void helper(List<Integer> ret, int[] arr, boolean[] visited, StringBuilder sb) {
        if (sb.length() == arr.length) {
            ret.add(Integer.parseInt(sb.toString()));
            return;
        }

        for (int i = 0; i < arr.length; i++) {
            if (sb.length() == 0 && arr.length > 1 && arr[i] == 0) continue;
            int len = sb.length();
            if (!visited[i]) {
                sb.append(arr[i] + "");
                visited[i] = true;
                helper(ret, arr, visited, sb);
                sb.setLength(len);
                visited[i] = false;
            }
        }
    }

    public static void main(String[] args) {
        //ComposeInteger a = new ComposeInteger();
        int[] arr = {4, 0, 2, 3};
        List<Integer> ret = getIntegers(arr);
        System.out.print(ret);
    }
}
