import java.util.*;

public class WorldBreak {

    //require: each world in wordDict should only be used at most once
    public boolean wordBreak(String s, List<String> wordDict) {
        //Set<String> set = new HashSet<>(wordDict);
        Map<String, Boolean> wordMap = new HashMap<>();
        for (String w: wordDict) {
            wordMap.put(w, false);
        }
        return helper(s, 0, wordMap);
    }

    Map<Integer, Boolean> map = new HashMap<>();
    private boolean helper(String s, int idx, Map<String, Boolean> wordMap){
        if (idx >= s.length()) return true;
        if (map.containsKey(idx)) {
            return map.get(idx);
        }

        boolean ret = false;
        for (int i = idx; i < s.length(); i++) {
            String st = s.substring(idx, i+1);
            if (wordMap.containsKey(st) && !wordMap.get(st)) {
                wordMap.put(st, true);
                if (helper(s, i + 1, wordMap)) {
                    ret = true;
                    break;
                }
                wordMap.put(st, false);
            }
        }
        map.put(idx, ret);
        return ret;
    }

    public static void main(String[] args){
        WorldBreak o = new WorldBreak();
        String s = "applepenapple";
        List<String> wordDict = new ArrayList<>();
        wordDict.add("apple");
        wordDict.add("pen");
        wordDict.add("egg");
        boolean ret = o.wordBreak(s, wordDict);
        System.out.println(ret);

    }
}
