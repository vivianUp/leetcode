import java.util.Objects;

public class WoodCut {
    private String t1;
    private int t2;

    public static int LongestWoodLength (int[] L, int k) {

        int low = 1;
        int high = 1;
        for (int len: L) {
            high = Math.max(high, len);
        }

        while (low + 1 < high) {
            int mid = low + (high - low) / 2;
            int cnt = getCnt(L, mid);
            if (cnt >= k) {
                low = mid;
            } else {
                high = mid;
            }
        }
        if (getCnt(L, high) >= k) {
            return high;
        } else if (getCnt(L, low) >= k) {
            return low;
        }
        return 0;
    }

    private static int getCnt(int[] L, int len){
        int ret = 0;
        for (int a: L) {
            ret += a / len;
        }
        return ret;
    }


    public static void main(String[] args){
        int[] L = {232, 124, 456};
        int k = 7;
        System.out.println(LongestWoodLength(L, k));
    }

    public String getT1() {
        return t1;
    }

    public int getT2() {
        return t2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WoodCut woodCut = (WoodCut) o;
        return t2 == woodCut.t2 &&
                Objects.equals(t1, woodCut.t1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(t1, t2);
    }

    @Override
    public String toString() {
        return "WoodCut{" +
                "t1='" + t1 + '\'' +
                ", t2=" + t2 +
                '}';
    }
}
