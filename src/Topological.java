import java.security.InvalidParameterException;
import java.util.*;

public class Topological {

    public static boolean isValidOrder(String[][] arr) throws InvalidParameterException {
        if (arr == null) {
            throw new InvalidParameterException("no input");
        }
        if (arr.length == 0) return true;
        Map<String, Set<String>> map = new HashMap<>();
        Map<String, Integer> preCnt = new HashMap<>();
        for (String[] st: arr) {
            if (st[0].equals(st[1])) return false;//invalid input

            preCnt.putIfAbsent(st[0], 0);
            preCnt.putIfAbsent(st[1], 0);
            if (!map.containsKey(st[0])) {
                map.put(st[0], new HashSet<>());
            }
            map.get(st[0]).add(st[1]);
            preCnt.put(st[1], preCnt.get(st[1]) + 1);
        }

        Queue<String> queue = new LinkedList<>();
        for (String key: preCnt.keySet()) {
            if (preCnt.get(key) == 0) {
                queue.offer(key);
            }
        }

        int cnt = 0;
        while (!queue.isEmpty()) {
            String st = queue.poll();
            cnt++;
            if (map.containsKey(st)) {
                for(String follow: map.get(st)) {
                    preCnt.put(follow, preCnt.get(follow) -1);
                    if (preCnt.get(follow) == 0) {
                        queue.offer(follow);
                    }
                }
            }
        }

        return cnt == preCnt.size();

    }

    public static void main(String[] args) {
        //a > b, b > c, c > a;
        String[][] arr = {{"a", "b"}, {"b", "c"}, {"c", "a"}};
        System.out.println(isValidOrder(arr));
    }
}
