import java.util.*;

public class WordLadder {

    // Leetcode: 126. Word Ladder II
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        Set<String> dict = new HashSet<>(wordList);
        if (!dict.contains(endWord)) return new ArrayList<>();

        //bi-directional BFS, build a map
        //map key: a word; value: list of neighbors of the key
        Map<String, List<String>> map = new HashMap<>();
        Set<String> set1 = new HashSet<>();
        set1.add(beginWord);
        Set<String> set2 = new HashSet<>();
        set2.add(endWord);
        buildMap(set1, set2, map, dict, true);

        //dfs to print results
        List<List<String>> ret = new ArrayList<>();
        List<String> list = new ArrayList<>();
        list.add(beginWord);
        dfs(ret, list, map, beginWord, endWord);
        return ret;
    }

    private void buildMap(Set<String> set1, Set<String> set2, Map<String, List<String>> map, Set<String> dict, boolean forward) {
        dict.removeAll(set1);
        dict.removeAll(set2);
        if (set1.size() > set2.size()) {
            buildMap(set2, set1, map, dict, !forward);
            return;
        }

        boolean done = false;
        Set<String> set3 = new HashSet<>();
        for (String st: set1) {
            for (int i = 0; i < st.length(); i++) {
                char ch = st.charAt(i);
                for (char c = 'a'; c <= 'z'; c++) {
                    if (c != ch) {
                        StringBuilder sb = new StringBuilder(st);
                        sb.setCharAt(i, c);
                        String tmp = sb.toString();
                        if (set2.contains(tmp) || dict.contains(tmp)) {
                            if (set2.contains(tmp)) {
                                done = true;
                            } else {
                                set3.add(tmp);
                            }
                            //build map
                            String key = forward ? st : tmp;
                            String val = forward ? tmp: st;
                            if (!map.containsKey(key)) {
                                map.put(key, new ArrayList<>());
                            }
                            map.get(key).add(val);
                        }
                    }
                }
            }
        }
        if (!done && set3.size() != 0) {
            buildMap(set3, set2, map, dict, forward);
        }
    }


    private void dfs(List<List<String>> ret, List<String> list, Map<String, List<String>> map, String start, String end) {
        if (start.equals(end)) {
            ret.add(new ArrayList<>(list));
            return;
        }
        if (!map.containsKey(start)) return;
        for (String st: map.get(start)) {
            list.add(st);
            dfs(ret, list, map, st, end);
            list.remove(list.size() -1);
        }
    }
}
