package Google;

/*
* https://leetcode.com/discuss/interview-question/352459/
* */
public class LargestSubarray {
    public int[] largestSubarray(int[] arr, int k) {
        if (k == arr.length) {
            return arr;
        }
        int[] ret = new int[k];
        int start = 0;
        for (int i = 0; i <= arr.length - k; i++) {
            for (int j = 0; j < k; j++) {
                if (arr[i+j] > arr[start + j]) {
                    start = i;
                    break;
                }
            }
        }

        for(int i = 0; i < k; i++) {
            ret[i] = arr[start + i];
        }
        return ret;
    }
}
