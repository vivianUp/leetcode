package Google;
/*
* https://leetcode.com/discuss/interview-question/394347/
* */

public class Watering {
    public int watering(int[] plants, int cap1, int cap2) {
        int ret = 0;
        int amt1 = 0;
        int amt2 = 0;
        int i = 0;
        int j = plants.length - 1;
        while (i < j) {
            if (plants[i] > amt1) {
                ret++;
                amt1 = cap1;
            }
            if (plants[j] > amt2) {
                ret++;
                amt2 = cap2;
            }
            amt1 -= plants[i];
            amt2 -= plants[j];
            i++;
            j--;
        }
        if (i == j && plants[i] > amt1 + amt2) {
            ret++;
        }
        return ret;
    }
}
