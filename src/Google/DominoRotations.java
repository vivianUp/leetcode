package Google;

// Leetcode 1007. Minimum Domino Rotations For Equal Row

public class DominoRotations {
    public int minDominoRotations(int[] A, int[] B) {
        if (A.length == 1) return 0;
        int cntSwap1 = helper(A, B, A[0]);
        int cntSwap2 = helper(A, B, B[0]);
        int ret = Math.min(cntSwap1, cntSwap2);
        return (ret == Integer.MAX_VALUE) ? -1 : ret;
    }

    private int helper(int[] A, int[] B, int val) {
        int len = A.length;
        int cntA = 0;
        int cntB = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] != val && B[i] != val) {
                break;
            }
            if (A[i] == val && B[i] == val) {
                len--;
            } else if (A[i] == val) {
                cntA++;
            } else {
                cntB++;
            }
        }
        if (cntA + cntB == len) {
            return Math.min(cntA, cntB);
        }
        return Integer.MAX_VALUE;
    }
}
