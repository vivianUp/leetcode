public class SearchRange {
    /*
    / *********binary search 变种题*********
    https://yeqiuquan.blogspot.com/2016/03/search-in-big-sorted-array.html

[LintCode] 447 Search in a Big Sorted Array 解题报告
Description
Given a big sorted array with positive integers sorted by ascending order. The array is so big so that you can not get the length of the whole array directly, and you can only access the kth number by ArrayReader.get(k) (or ArrayReader->get(k) for C++). Find the first index of a target number. Your algorithm should be in O(log k), where k is the first index of the target number.

Return -1, if the number doesn't exist in the array.

Notice
If you accessed an inaccessible index (outside of the array), ArrayReader.get will return 2,147,483,647.


Example
Given [1, 3, 6, 9, 21, ...], and target = 3, return 1.

Given [1, 3, 6, 9, 21, ...], and target = 4, return -1.


Challenge
O(log k), k is the first index of the given target number.

思路
反方向先找到一个超过target的index。把index -1设为end，然后进行二分查找。由于可能有一堆一样的target，所以我们需要找第一个出现的target。
    *
    *
    * */


    //34. Find First and Last Position of Element in Sorted Array
    public int[] searchRange(int[] nums, int target) {
        int[] ret = new int[2];
        ret[0] = -1;
        ret[1] = -1;
        if (nums == null || nums.length == 0) {
            return ret;
        }
        int left = search(nums, target);
        if (left >= nums.length || nums[left] != target) {
            return ret;
        }
        ret[0] = left;
        int right = search(nums, target + 1);
        ret[1] = right - 1;
        return ret;
    }

    //return the first position (the first position >= target) if target val is inserted into the array
    private int search (int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (left + 1 < right) {
            int mid = left + (right - left) /2;
            if (target <= nums[mid]) {
                right = mid;
            } else {
                left = mid;
            }
        }

        if (target <= nums[left]) {
            return left;
        } else if (target <= nums[right]){
            return right;
        } else {
            return right + 1;
        }
    }
}
