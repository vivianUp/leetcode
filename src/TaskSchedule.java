import java.util.*;

class Task {
    char name;
    int cnt;
    public Task(char name) {
        this.name = name;
        cnt = 0;
    }
}


public class TaskSchedule {
    //question2: don't need to maintain the original order, print put the output path
    public static int schedule2(char[] tasks, int n) {
        Task[] arr = new Task[26];
        for (char t: tasks) {
            if (arr[t-'A'] == null) {
                arr[t-'A'] = new Task(t);
            }
            arr[t-'A'].cnt++;
        }
        PriorityQueue<Task> pq = new PriorityQueue<>(new Comparator<Task>(){
            public int compare(Task a, Task b) {
                if (a.cnt == b.cnt) {
                    return a.name - b.name;
                } else {
                    return b.cnt - a.cnt;
                }
            }
        });

        for (int i = 0; i < 26; i++) {
            if (arr[i] != null) {
                pq.offer(arr[i]);
            }
        }

        StringBuilder sb = new StringBuilder(); //store the path
        int ret = 0;
        while (!pq.isEmpty()) {
            List<Task> tmp = new ArrayList<>();
            for (int i = 0; i <= n; i++) {
                ret++;
                if (!pq.isEmpty()) {
                    Task cur = pq.poll();
                    sb.append(cur.name);
                    if (--cur.cnt > 0) {
                        tmp.add(cur);
                    }
                    if (pq.isEmpty() && tmp.size() == 0) break;
                } else {
                    sb.append("*");
                }
            }
            for (Task t: tmp) {
                pq.offer(t);
            }
        }
        System.out.println(sb.toString());
        return ret;
    }





    //question 1: maintain the original order of tasks, output the path
    public static String schedule1(char[] arr, int n) {
        Map<Character, Integer> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        int idx = 0;
        for (int i = 0; i < arr.length; i++) {
            char ch = arr[i];
            if (map.containsKey(ch)) {
                int lastSeen = map.get(ch);
                idx = Math.max(idx, lastSeen + n + 1);
            }
            while (sb.length() < idx) {
                sb.append('#');
            }
            sb.append(ch);
            map.put(ch, idx);
            idx++;
        }
        return sb.toString();
    }


    //question 1: maintain the original order of tasks
    public static int schedule(char[] arr, int n) {
        Map<Character, Integer> map = new HashMap<>();
        int idx = 0;
        for (int i = 0; i < arr.length; i++) {
            char ch = arr[i];
            if (map.containsKey(ch)) {
                int lastSeen = map.get(ch);
                idx = Math.max(idx, lastSeen + n + 1);
            }
            map.put(ch, idx);
            idx++;
        }
        return idx;
    }


    public static void main(String[] args) {
        char[] arr = {'A', 'A', 'A', 'A', 'B', 'C', 'B', 'C'};
        int n = 3;
        int ret = schedule2(arr, n);
        //String retTasks = schedule1(arr, n);
        System.out.println(ret);
        //System.out.println(retTasks);
    }
}
