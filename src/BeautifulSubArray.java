import java.util.LinkedList;
import java.util.Queue;

public class BeautifulSubArray {

    //corner case: m == 0
    public static int getBeautifulSubArray(int[] arr, int m) {
        Queue<Integer> oddIdxQueue = new LinkedList<>();
        int left = 0;
        int ret = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                oddIdxQueue.offer(i);
            }
            if (oddIdxQueue.size() > m) {
                left = oddIdxQueue.poll() + 1;
            }
            if (oddIdxQueue.size() == m) {
                if (m == 0) {
                    ret += i - left + 1;
                } else {
                    ret += oddIdxQueue.peek()- left + 1;
                }
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        int[] arr = {2, 5, 4, 9};
        int m = 2;
        int cnt = getBeautifulSubArray(arr, m);
        System.out.print(cnt);
    }
}
