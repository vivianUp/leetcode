import java.util.HashMap;
import java.util.Map;

class LeastInterval{
    //tasks should be done in original order, there should be n intervals between the same tasks
    public static int getLeastInterval (char[] tasks, int n){

        int idx = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (char ch: tasks) {
            if (!map.containsKey(ch)) {
                map.put(ch, idx);
                idx++;
            } else {
                if (map.get(ch) + n+1 > idx) {
                    int tmpIdx = map.get(ch) + n + 1;
                    map.put(ch, tmpIdx);
                    idx = tmpIdx +1;
                } else {
                    map.put(ch, idx);
                    idx++;
                }
            }
        }

        return idx;
    }

    public static void main(String[] args){

        char[] tasks = {'A', 'A', 'A', 'B', 'A', 'B'};
        int len = getLeastInterval(tasks, 3);
        System.out.println(len);
    }
}


